#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "main.h"
#include "foo.h"

int main(int argc, char **argv) {

    int x = 0;
    char *end = NULL;

    if (argc < 2) {
        fprintf(stderr, "error: %s\n", NO_ARG_ERROR);
        exit(1);
    }

    x = strtol(argv[1], &end, 10);
    if (end == argv[1]) {
        fprintf(stderr, "error: '%s' %s\n", argv[1], NO_NUM_ERROR);
        exit(1);
    }

    foodo(x);

}
