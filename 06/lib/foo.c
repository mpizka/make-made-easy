#include <stdio.h>

#include "foo.h"

void foodo(int x) {
    int y = x * FOONUM;

    printf("foodoo: %d x %d = %d\n", x, FOONUM, y);
}
