# make made easy

The `make` program is the goto build-system for many projects in C and C++. It
is also a daunting topic, especially for beginners, not in the least because it
is usually presented in interaction with the `gcc` compiler, which is an
enormously complex topic in itself.

Because of this, we will start this tutorial by divorcing `make` of the
intricacies of any real compiler, and start with an easy-to-understand
substitute, to which we add features as our understanding of `make`'s
capabilities grows.

It is our hope that this will not only make the basics of `make` easier to
digest, but also showcase that make can work with many different programs and
scenarios, not all of which have to be compilers and linkers.


# License and Contact

This work is released into the public domain under the terms of _The
Unlicense_. See [LICENSE][license] for more info.

If you want to contribute to this work, feel free to send a pull request, or
contact me at mpizka@icloud.com


# Steps

We want to explain the topic in easily digestible steps, consecutively adding
more features to a makefile, explaining new topics as we go, and asking
questions leading into the next step.

All steps will be self-contained in their respective directory; eg. the files
for step 01 will be found in `01/` and so forth.

Not all the explanation for each step will be in this readme. This tutorial is
about `make`, and reading the makefiles is essential to understand what
happens/changed in each step and why. We therefore put a lot of information in
the comments of each steps makefile.

Almost each step begins with a little introduction in this readme, followed by
a list of tasks you should perform to familiarize yourself with the material.
It is recommended to read the `Makefile` after the initial introduction.

> Note: Step 00 doesn't have a `Makefile`


## 00 - setup

In the first step, we present the Substitutes we use as stand ins for an actual
compiler and linker.

The script `compiler` takes in `*.src` (source) files, and outputs `*.obj`
(object) files, adding the contents of the file `special.sauce` if it is
present.

    compiler *.src

The script `linker` will "build" the final output file from the object-files,
using its first argument as the output file name. The result is a shell script
simply printing the "compiled" output".

    linker bin *.obj

1. Run the above commands in the `00/` directory

2. Run `bin`

3. Edit the file `special.sauce` and do a "recompilation". Note the change in
   the object files.

4. Edit a source-file and run the compiler/linker again. Do you need to use the
   above commands, or would a subset of commands suffice?

5. Look at the directory after the commands. What needs to be done to clean up?


## 01 - make basics

The above examples demonstrate some of the pain-points that build systems like
`make` help us to deal with:

* Building projects can include many individual commands, and issueing them in
  the correct sequence, with the correct arguments is something we definitely
  want to automate

* During development we often only introduce  partial changes to our code that
  do not warrant a complete re-compilation. Figuring out which commands we need
  to re-run is a hassle

* There are tasks surrounding builds (installation, cleanup, packaging, ...)
  that we could want to automate

> For smaller projects it's usually fine to just re-run all the commands after
> changes. However, with larger projects, doing only the necessary work becomes
> important; eg. the Linux Kernel can take hours to compile from scratch.

`make` and similar tools allow us to make this process easier, by specifying
"recipies" for the above tasks. A makefile is a collection of such recipies.

1. Look at `Makefile` in an editor

2. Run `make`

3. Run `make` again, compare the output

4. `touch foo.src` or change the file in an editor. Run `make` again, and
   compare the output with the one from 1

5. `touch foo.obj` or change the file in an editor. Run `make` again, and
   compare the output with the one from 1 and 4

6. Run `make clean`

When executed without arguments, `make` will, look for a file `makefile` or
`Makefile` in the current directory and execute the commands within. You can
specify an alternate makefile with the `-f` flag

When invoked without an argument, `make` will run the first rule in the file,
which is often called `all`. If a rule has dependencies, they are interpreted 
as targets of other rules, which are then run recursively. This way, `make`
solves the first of our challenges; running the necessary commands of a build
in the correct sequence. In a way, the Makefile describes the build process in
reverse; We say what we want to build, and what is needed to build it; then we
say how each of those parts is built and what it requires, and so on...

Whether a rule has to run is determined by a very simple, but effective, logic;
If there is a file with the same name as the rules target, and that file was
modified after the files of it's dependencies, the rules commands don't need to
run. Otherwise, `make` runs them.

> The `touch` program simply sets the modified date of a file to the current
> time. You can access this information by running `stat FILE`.

Since the rule `all` doesn't have a file, it always runs. However, when the
rule for `$(BINARY)` is checked, `make` determines that `bin` is "younger" than
its dependencies, and so nothing has to be done. This check is done recursively
down the graph of rules depending on each other, which is why `touch foo.src`
triggered a partial re-build, even though `bin` was still younger (newer?) than
all the `*.obj` files.

> The file-checking is dependent on a rule having commands. You can try `touch
> all`, it doesn't change the behavior of make, until you add any command to
> the rule. Try adding the `true` command to the rule and see what happens.

Lastly for this step; since `make` can execute arbitrary shell commands, we can
also define rules that perform tasks surrounding builds for us, such as
configuration or cleanup. Notable examples you see often in the wild, are the
`clean` rule (which we show here), and the `install` rule, that moves the
ready-built files to their intended destination(s) in the file system. The
latter is often accompanied by a rule to uninstall a software as well.

> Note: Before the widespread appearance of package-managers, this was one of
> the most common methods to install software on unixoid systems. Almost
> everyone, even if they didn't know how `make` works, knew the _"magic"_
> command `make && make install`


## 02 - A slightly better makefile

In 01 we specified the `*.obj` files directly in the rule for the binary. This
is perfectly okay for smaller projects, but gets tedious quickly once the
codebase grows, since we would need to add new files into the rules all the
time.

It's also not really intuitive to specify dependencies that are build-artifacts
(object-files), instead of the source-files that we edit.

This version of our makefile takes care of that by introducing the power of
some of `make`s builtin functions, to auto-detect source files and figure out
what object files they would result in.

1. Read the `Makefile`

2. Run `make print`

3. Run `make`

4. Add a new file `moo.src` to the project. Re-run `make print` and `make`

5. Run `make pkg` and `make clean`

We also improved some other details;

The `clean` rules `rm` commands have been given the `-f` flag. Otherwise, they
would fail if a file isn't present, and block the downstream clean-commands
from executing. Similar to other POSIX commands, `make` considers a command as
failed, if its return value != 0.

> Try to make a rule fail by adding the `false` command to it.

We also added the special rule defining the `.PHONY` target, and listing
`clean` and `pkg` as it's dependencies. This tells make that these rules should
always run, even if there is (for whatever reason) a file named the same as
their target.

> Comment the `.PHONY` rule out, `touch clean` and try to run `make clean`

We will touch on some more functions in the  next steps, but there are alot
more which we will not cover in this tutorial. If you are interested, almost
every mainstream distro comes with the documentation of make in the form of an
info-file;

    info make


## 03 - An even better makefile

Using wildcards to find our sources is nice, but in larger projects, our
sources may be spread over several directories. It would be nice if we could
just list those, and have the build system figure out the rest.

That's exactly what we did here: A new variable `SRCDIRS` lists all directories
where `*.src` files are to be found, and a `foreach` loop builds our filelist
from there.

Using command line arguments to `make` and envvars, we can add additional
directories on the fly. Permanent additions to the project can of course be put
into the SRCDIRS variable in the makefile.

1. Read `Makefile`

2. Run `make`

3. Add new `*.src` files to `03/` and `03/lib/`, run `make` again

4. Add a new directory `code`, populate it with some source-files

5. Run `make print OPTDIRS=code`

6. Run `ENVDIRS=code make print`

Since we can now use envvars and command line arguments to change the behavior
of `make`, what else could we control with these? Basically...everything!
Compilerflags guiding conditional compilations are an obvious example; Want to
turn on an optional feature? Specify the list in a compiler flag to `make`.

Another thing; remember `special.sauce`? Yeah, the file that the "compiler"
adds into every `*.obj` file it creates. So far, changing that file was no
reason for make to rebuild anything (go on, try it in step 02).

That's because `special.sauce` wasn't listed as a dependency in any rule. And
there is a good reason for that: In the rule that generates our `*.obj` files,
we used the list of dependencies as argument to the "compiler". Listing
`special.sauce` would have resulted in an error (the compiler only accepts
`*.src` files). 

One way to solve this is demonstrated here with the change in the `%.obj` rule:
`special.sauce` is now listed as a dependency for every object-file, but not
used as argument to the "compiler".


## 04 - More complicated dependencies

The solution to the `special.sauce` problem in step 03 is fine, because when
`special.sauce` changes, it affects all compilations, so it should be a
dependency of every object.

But what if that isn't the case? For this step, we have changed `compiler` a
bit, making it so it no longer adds a global special sauce, but adds sauce
listed in the `*.src` files themselves.

Source files may include lines like this;

    add foo.sauce
    add bar.sauce

; which the compiler will insert into the object file accordingly. If this
seems familiar, it's because it is: The C preprocessor does pretty much this
when it processes an `#include` command.

1. Look at `compiler` (if you want to) and compare it with the one from step 03

2. Read `Makefile`

3. Look at the changed rule for object-files in `makefile`

4. Run `make`

5. Look at the object-files generated.

6. Change a `*.sauce` file, run `make` again. Did anything happen? Why not?

You may have noticed that this presents us with a problem in the makefile; how
do we list these dependencies, which are defined in the source files? The
easiest way would be to just make a listing (or variable) of all sauce-files
and adding that to the relevant rule. Try modifying the rule like so;

    %.obj: %.src sauces/*.sauce main.sauce
        compiler $<

; and `touch main.sauce` then `make`. Okay, seems to work, but: now we rebuild
_all_ the libs whenever any `*.sauce` file changes, eg. we rebuilt both
`foo.obj` and `bar.obj` after `touch`ing `main.sauce`, even though neither
`foo.src` nor `bar.src` list it as a dependency. So we effectively eliminated
one of the major reasons why we use a build-system; the ability to easily do
partial rebuilds.

Still, this is fine for most medium-sized projects. Non-source file
dependencies (eg. header files) change less frequently than sources do, so for
most projects, you are gonna be fine just by putting all header files in the
dependencies, because  total-rebuild caused by changing a header won't happen
that often.

But of course, there has to be a more elegant way.


## 05 - A little help?

What if we could list the exact dependencies for every `*.src` file seperately
in our `Makefile`? That would solve the rebuild problem (when changing a
`*.sauce` we would only rebuild the files that `add` it).

Problem is, we cannot easily automate this from the names of the `*.src` files,
the way we do with our `*.obj` files; a source file may `add` (include) an
arbitrary number of arbitrarily named sauces. So, we would need to do the
listing by hand; building a rule/target for every single source file. That
would be tedious and error-prone, not to mention, defeat the automation we
enjoyed so far.

But what if we didn't have to do this ourselves?

In this step, we again changed our `compiler` a bit. It can now take an
optional flag `-m`, which causes it to write a `*.mh` (Make-help) file. This
file consists of a single rule, specifying the exact requirements to build the
object-file from the source file the compiler is currently working on, eg.
`sources/foo.mh`;

    sources/foo.obj: sources/foo.src sources/../sauces/all.sauce sources/../sauces/foo.sauce
            compiler -m $<

Okay, but how do we get these files into our makefile? And doesn't this create
a kind of hen & egg problem, aka. we have to use the compiler to make the rules that
help make use the compiler - kinda thing?

We put these files into the makefile using an `-include` directive;

    -include $(HELPERS)

Remember that each helper-file holds a rule for a specific object-file, eg. the
file `main.mh` will hold a rule saying "Making main.obj depends on main.src and
main.sauce, and is done by invoking the compiler like this ..."

Also remember that we have a wildcard-rule for all `*.obj` files in our
makefile. Since the specific rules are `-include`d _after_ the wildcard rule
has been specified, they _override_ the wildcard rule for their respective
file.

1. Look at the changes in `compiler` and read  `Makefile` 

2. Run `make`, then run it again

3. Go through `main.mh` and `sources/foo.mh`

3. `touch main.sauce` and run `make` again

4. `touch sauces/all.sauce` and run `make` again.

5. Add new source & sauces files and run `make` again

6. Run `make print` and `make clean`

So when we run `make` for the first time, no helper files exist, the `-include`
directive changes nothing, and all `*.obj` files get build by using the
wildcard rule.

But in following `make` invocations, we suddenly have specific rules for each
object-file, which were built automatically by a program that knows how to read
what dependencies a source file has; the compiler. These file-by-file specific
rules include more detailed dependency information (aka. they list the
`*.sauce` files each file needs), and so changing a `*.sauce` file causes a
rebuild of only those object-files that depend on it, in subsequent invocations
of `make`.

If we were to include new source files into our scheme afterwards, they would
first be built using the catch-all rule, and then fall under the same schema.

> Try adding a new source file that `add`s `sauces/all.sauce`. Run make, then
> `touch` the new source file and run `make` again. Explain the difference in
> the output.

What if we `add` a new sauce to an existing source? If you look at the rules
specified in the `*.mh` files, you will notice that the `compiler` invocation
they emit includes the `-m` flag. So, when we add a new sauce to a source, the
source becomes newer than the target (obj-file) that depends on it, so the
compilation step runs, and the `*.mh` file is updated.

> Check `main.mh`. Change `main.src` so it `add`s `sauces/all.sauce`. Run
> `make` again. Observe the change in `main.mh`.

Okay, sounds good; one question tho, doesn't that mean that, during the first
build, the object-files are built without `make` knowing about their sauce-file
dependencies?

Yes, that's absolutely true. It's also irrelevant; Since the object files don't
exist before the first `make` invocation, there is no danger of the build
ignoring any changes to the sauce-files; it has to build everything from
scratch anyway.

And of course, we are not just giving arbitrary examples here; a far more
sophisticated version of the mechanism described above, is the exact reason why
`gcc` has the `-M` family of flags.


# 06 - Let's get real

At this stage, there is really no point in using stand-ins for the real thing
any more. We gradually built up the complexity of what our makefile and
"compiler/linker" do, so now we can take off the training wheels and use `make`
with a real compiler.

The application built here takes a numerical argument and prints its multiple
with a constant defined in `foo.h`. eg.;

    $ bin 42
    foodoo: 21 x 2 = 42

Alot of this makefile should make sense immediately now.  Some things are
specific to C and `gcc`;

* Obviously, we have `*.c *.o` instead if `*.src *.obj`

* Instead of adding sauce, we include headers (`*.h` files)

* There is no seperate `linker` invocation; `gcc` does that for us
  automatically when building the binary from the object files

* We define the `INCLUDES` variable, which will hold all the `-I...` flags we
  need to pass to `gcc`. These are the additional directories that `gcc` will
  look in for header files

* We define the `CC` (C-Compiler) and `CFLAGS` (Compiler-Flags) variables and
  use them in our rules commands

We will come back to `CC` and `CFLAGS` in step 07. For now the most interesting
thing are the `DEPENDS`, which we treat the same as our `HELPERS` before. Where
do we make these?

Well, same as with our stand in, it's the compiler (`gcc`) doing that for us.
This is the reason for the `-MP` and `-MD` flags. You can look up what these do
exactly on `man gcc` (there are some other options of the `-M` family as well),
but essentially these work in a way similar to our toy-compilers `-m` flag
before; they instruct `gcc` to emit files containing specific rules for each
object file that gets compiled from source.

0. Read the source and header files

1. Read `Makefile`

2. Run `make print`

2. Run `make`, run it again

3. `touch lib/foo.o` and run `make`

4. `touch main.c` and run `make`

5. Change one of the errorstrings in `includes/main.h` and run `make`

5. `touch foo.h`, or better yet, change `FOONUM` to `4`

6. Run `make` again, and explain the difference with the output from 5

7. Read one of the `*.d` files (our headers will be at the very end)



<!--Links-->

[license]: https://gitlab.com/mpizka/make-made-easy/blob/main/LICENSE
[pass-variables]: https://stackoverflow.com/questions/2826029/passing-additional-variables-from-command-line-to-make
