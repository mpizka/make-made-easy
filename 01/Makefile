# A very basic makefile

# --------
BINARY=bin
# --------
# This is a VARIABLE DEFINITION
# Variables are used by prefixing them with $ or writing them in $(...)
# `make` also specifies a couple of special variables, among those are;
#
# 	$@	The target of a rule
# 	$^	All dependencies of a rule
# 	$<	The first dependency of a rule


all: $(BINARY)


# -------------------------
$(BINARY): foo.obj bar.obj
	linker $@ $^
# -------------------------
# This is a RULE.
# It's basic syntax is
#
# target: [ dependency... ]
# 	[ command... ]


# -------------
%.obj: %.src
	compiler $^
# -------------
# This rule uses a STATIC PATTERN in both its target and dependencies. Written
# in plain english, this rule says: "Generating X.obj depends on X.src; to
# generate X.obj, run `compiler X.src`". In our example, I could have written;
#
# 	foo.obj: foo.src
# 		compiler foo.src
# 	bar.obj: bar.src
# 		compiler bar.src
# 
# ; for an equivalent effect.


# --------------
clean:
	rm *.obj
	rm $(BINARY)
# --------------
# This rule isn't associated with a file, and isn't (directly) involved with
# the build-process. But, since you have likely aready figured out that many
# commands in rules are just instructions for the shell, we can make use of
# this to let make take care of arbitrary tasks, such as cleanup, installation,
# packaging, ...
